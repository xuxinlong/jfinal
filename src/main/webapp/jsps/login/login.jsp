<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>后台管理系统</title>
	<link rel="stylesheet" type="text/css" href="${ctx}/include/css/bglogin.css" />
	<script src="${ctx}/include/plugins/jquery/jquery-1.11.1.min.js"></script>
</head>
<body>
	<div>
	<form id="loginForm" action="${ctx}/login/login" method="post" >
		<!-- 防止重复提交的token -->
		${token}
		<div class="login_top">
			<div class="login_title">
				第6通道门店管理后台 
			</div>
		</div>
		<div style="float:left;width:100%;">
			<div class="login_main">
				<div class="login_main_top"></div>
				<div class="login_main_ln">
					<input type="text" id="username" name="username" />
				</div>
				<div class="login_main_pw">
					<input type="password" id="password" name="password"/>
				</div>

				<div class="login_main_remb">
				<!--	<input id="rm" name="rememberMe" type="hidden"/> <label for="rm"><span>记住我</span></label> -->
				</div>
				<div class="login_main_submit">
					<button onclick="checkForm()" type="button"></button>
				</div>
			</div>
		</div>
	</form>
	</div>
</body>
</html>
<script>
$(function(){
	$("#loginForm").keydown(function (event){
		if(event.keyCode==13){
			checkForm();
		}
	});
})
function checkForm() {
    if ($.trim($('#username').val()).length < 1) {
        alert("用户名不能为空!");
        $('#username').focus();
        return;
    }
    if ($.trim($('#password').val()).length < 1) {
        alert("密码不能为空!");
        $('#password').focus();
        return;
    }
    $.ajax({
    	url:"${ctx}/login/login",
    	type:"post",
    	data:$("#loginForm").serialize(),
    	success:function(data){
    		if(data.errorCode=="y"){
    			location.href="${ctx}/index";
    		}else{
    			alert(data.errorText);
    		}
    	}
    })
}
</script>