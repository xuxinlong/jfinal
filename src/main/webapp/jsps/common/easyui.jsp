<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- easyui皮肤 -->
<link href="${ctx}/include/plugins/easyui/theme/<c:out value="${cookie.themeName.value}" default="default"/>/easyui.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/include/plugins/easyui/theme/icon.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/include/plugins/easyui/icons/icon-all.min.css" rel="stylesheet" type="text/css" />
<!-- 公共css与js -->
<link href="${ctx}/include/plugins/easyui/common/common.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/include/js/common.js" type="text/javascript"></script>

<script src="${ctx}/include/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="${ctx}/include/plugins/easyui/jquery-easyui-1.3.6/jquery.easyui.min.js" type="text/javascript"></script>
<script src="${ctx}/include/plugins/easyui/jquery-easyui-1.3.6/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>

<!-- easyui插件扩展 -->
<script src="${ctx}/include/plugins/easyui/release/jquery.jdirk.min.js" type="text/javascript"></script>


<link href="${ctx}/include/plugins/easyui/jeasyui-extensions/jeasyui.extensions.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/include/plugins/easyui/jeasyui-extensions/jeasyui.extensions.js" type="text/javascript"></script>
<script src="${ctx}/include/plugins/easyui/jeasyui-extensions/jeasyui.extensions.tabs.js" type="text/javascript"></script>
<script src="${ctx}/include/plugins/easyui/jeasyui-extensions/jeasyui.extensions.theme.js" type="text/javascript"></script>

<script src="${ctx}/include/plugins/easyui/jeasyui-extensions/jeasyui.extensions.datagrid.js" type="text/javascript"></script>
<script src="${ctx}/include/plugins/easyui/jeasyui-extensions/jquery.edatagrid.js"></script>
<script src="${ctx}/include/plugins/easyui/jeasyui-extensions/jeasyui.extensions.validatebox.js" type="text/javascript"></script>
<script src="${ctx}/include/plugins/easyui/jeasyui-extensions/jeasyui.extensions.ty.js"></script>
<script src="${ctx}/include/plugins/easyui/jeasyui-extensions/jeasyui.extensions.combo.js" type="text/javascript"></script>


