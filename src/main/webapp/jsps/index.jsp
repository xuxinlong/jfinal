<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>第6通道门店管理后台</title>
<%@ include file="/jsps/common/easyui.jsp"%>
<script src="${ctx}/include/js/index-menu.js"></script>

<!--导入首页启动时需要的相应资源文件(首页相应功能的 js 库、css样式以及渲染首页界面的 js 文件)-->
<script src="${ctx}/include/plugins/easyui/common/index.js" type="text/javascript"></script>
<link href="${ctx}/include/plugins/easyui/common/index.css" rel="stylesheet" />
<script src="${ctx}/include/plugins/easyui/common/index-startup.js"></script>
</head>
<body>
	<!-- 容器遮罩 -->
    <div id="maskContainer">
        <div class="datagrid-mask" style="display: block;"></div>
        <div class="datagrid-mask-msg" style="display: block; left: 50%; margin-left: -52.5px;">
            正在加载...
        </div>
    </div>
    <div id="mainLayout" class="easyui-layout hidden" data-options="fit: true">
        <div id="northPanel" data-options="region: 'north', border: false" style="height: 80px; overflow: hidden;">
            <div id="topbar" class="top-bar">
                <div class="top-bar-left">
                   <div style="float:left"> <img src="${ctx}/include/images/dst.png" style="height:40px;margin-right:10px"></img></div>
                  <div style="float:left">  <h1 style="margin-left: 10px; margin-top:0px;color: #fff;font-size: 16px;line-height: 50px">第6通道门店管理后台<span style="color: #3F4752"></span></h1></div>
                </div>
                <div class="top-bar-right">
                    <div id="timerSpan"></div>
                    <div id="themeSpan">
                        <a id="btnHideNorth" class="easyui-linkbutton" data-options="plain: true, iconCls: 'layout-button-up'"></a>
                    </div>
                </div>
            </div>
            <div id="toolbar" class="panel-header panel-header-noborder top-toolbar">
                <div id="infobar">
                    <span class="icon-hamburg-user" style="padding-left: 25px; background-position: left center;">
                       ${user.nickName}，您好
                    </span>
                </div>
               
                <div id="buttonbar">
                     <span>更换皮肤：</span>
                    <select id="themeSelector"></select>
                  
                    <a href="javascript:void(0);" class="easyui-menubutton" data-options="menu:'#layout_north_set'" iconCls="icon-standard-cog">系统</a>  
                    <div id="layout_north_set">
                    	<div id="updatePassword"  onclick="updatePwd();">修改密码</div>
						<div id="btnFullScreen" data-options="iconCls:'key'">全屏切换</div>
						<div id="btnExit" data-options="iconCls:'logout'">退出系统</div>
					</div>
                    <a id="btnShowNorth" class="easyui-linkbutton" data-options="plain: true, iconCls: 'layout-button-down'" style="display: none;"></a>
                </div>
            </div>
        </div>

        <div data-options="region: 'west', title: '菜单导航栏', iconCls: 'icon-standard-map', split: true, minWidth: 200, maxWidth: 400" style="width: 220px; padding: 1px;">
            <div id="myMenu" class="easyui-accordion" data-options="fit:true,border:false">
            <c:forEach items="${menuList}" var="Pmenu">
            	<c:if test="${Pmenu.parentId==0}">
					<div title="${Pmenu.name}" style="padding: 5px;" data-options="border:false,iconCls:'${Pmenu.icon}'">
					<c:forEach items="${menuList}" var="Smenu">
						<c:if test="${Pmenu.id==Smenu.parentId}">
							<a id="btn" class="easyui-linkbutton" data-options="plain:true,iconCls:'${Smenu.icon}'" style="width:98%;margin-bottom:5px;" 
								onclick="addTab('${Smenu.name}','${ctx}/${Smenu.url}')">
								${Smenu.name}
							</a>
						</c:if>
					</c:forEach>
					</div>
				</c:if>
            </c:forEach>
			</div>
        </div>

        <div data-options="region: 'center'">
            <div id="mainTabs_tools" class="tabs-tool">
                <table>
                    <tr>
                        <td><a id="mainTabs_jumpHome" class="easyui-linkbutton easyui-tooltip" title="跳转至主页选项卡" data-options="plain: true, iconCls: 'icon-hamburg-home'"></a></td>
                        <td><div class="datagrid-btn-separator"></div></td>
						<td><a id="mainTabs_toggleAll" class="easyui-linkbutton easyui-tooltip" title="展开/折叠面板使选项卡最大化" data-options="plain: true, iconCls: 'icon-standard-arrow-out'"></a></td>
                        <td><div class="datagrid-btn-separator"></div></td>
                        <td><a id="mainTabs_refTab" class="easyui-linkbutton easyui-tooltip" title="刷新当前选中的选项卡" data-options="plain: true, iconCls: 'icon-standard-arrow-refresh'"></a></td>
                        <td><div class="datagrid-btn-separator"></div></td>
                        <td><a id="mainTabs_closeTab" class="easyui-linkbutton easyui-tooltip" title="关闭当前选中的选项卡" data-options="plain: true, iconCls: 'icon-standard-application-form-delete'"></a></td>
                    </tr>
                </table>
            </div>
            <div id="mainTabs" class="easyui-tabs" data-options="fit: true, border: false, showOption: true, enableNewTabMenu: true, tools: '#mainTabs_tools', enableJumpTabMenu: true">
                <div id="homePanel" data-options="title: '主页', iconCls: 'icon-hamburg-home'">
                    <div class="easyui-layout" data-options="fit: true">
                        <div data-options="region: 'north', split: false, border: false" style="height: 33px;">
                           	首页内容
                        </div>
                        <div data-options="region: 'center', border: false" style="overflow: hidden;">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div data-options="region: 'east', title: '日历', iconCls: 'icon-standard-date', split: true,collapsed: true, minWidth: 160, maxWidth: 500" style="width: 220px;">
            <div id="eastLayout" class="easyui-layout" data-options="fit: true">
                <div data-options="region: 'north', split: false, border: false" style="height: 220px;">
                    <div class="easyui-calendar" data-options="fit: true, border: false"></div>
                </div>
                <div id="linkPanel" data-options="region: 'center', border: false, title: '通知', iconCls: 'icon-hamburg-link', tools: [{ iconCls: 'icon-hamburg-refresh', handler: function () { window.link.reload(); } }]">
                    
                </div>
            </div>
        </div>


    </div>
<div id="dlg"></div>  
<script>

$('.easyui-linkbutton').on('click', function(){    
	$('.easyui-linkbutton').linkbutton({selected:false}); 
    $(this).linkbutton({selected:true});  
});

function updatePwd(){
	d=$("#dlg").dialog({   
	    title: '修改密码',    
	    width: 380,    
	    height: 340,    
	    href:'${ctx}/user/updatePwdPage',
	    maximizable:true,
	    modal:true,
	    buttons:[{
			text:'修改',
			handler:function(){
				$("#mainform").submit(); 
			}
		},{
			text:'取消',
			handler:function(){
					d.panel('close');
				}
		}]
	});
}
</script>
    
</body>
</html>
