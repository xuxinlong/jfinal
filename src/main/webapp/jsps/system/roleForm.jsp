<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
	<form id="mainform" action="${ctx}/role/save" method="post">
	<table  class="formTable">
		<tr>
			<td>角色名：</td>
			<td>
			<input type="hidden" name="id" value="${role.id}" />
			<input id="name" name="name" type="text" value="${role.name}" class="easyui-validatebox"  data-options="width: 150,required:'required'"/>
			</td>
		</tr>
		<tr>
			<td>角色编码：</td>
			<td><input id="roleCode" name="roleCode" type="text" value="${role.roleCode}" class="easyui-validatebox"  data-options="width: 150,required:'required'" validType="length[0,30]"/></td>
		</tr>
		<tr>
			<td>描述：</td>
			<td><textarea rows="3" cols="41" name="description" style="font-size: 12px;font-family: '微软雅黑'">${role.description}</textarea></td>
		</tr>
	</table>
	</form>
</div>
<script type="text/javascript">
$(function(){
	$('#mainform').form({    
	    onSubmit: function(){    
	    	var isValid = $(this).form('validate');
			return isValid;	// 返回false终止表单提交
	    },    
	    success:function(data){ 
	    	var data = eval('(' + data + ')');
	    	successTip(data,dg,d);
	    }    
	}); 
});
</script>
