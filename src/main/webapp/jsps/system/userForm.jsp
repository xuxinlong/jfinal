<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form id="mainform" action="${ctx}/user/save" method="post">
	<table class="formTable">
		<tr>
			<td>登录名：</td>
			<td>
				<input type="hidden" name="id" value="${user.id}"/>
				<input id="name" name="name" class="easyui-validatebox"  value="${user.name}" /> 
			</td>
		</tr>
		<tr>
			<td>名称：</td>
			<td><input id="nickName" name="nickName" class="easyui-validatebox" value="${user.nickName}" /></td>
		</tr>
		<tr>
			<td>电话：</td>
			<td><input name="mobile" value="${user.mobile}" class="easyui-numberbox" data-options="required:true"/></td>
		</tr>
		<tr>
			<td>信息描述：</td>
			<td><textarea rows="3" cols="41" name="info" >${user.info}</textarea></td>
		</tr>
	</table>
</form>
<script type="text/javascript">
//用户 添加修改区分
if("${user.id}"==''){
	//用户名存在验证
	$('#name').validatebox({    
	    required: true,    
	    validType:{
	    	length:[2,20],
	    	remote:["${ctx}/user/checkLoginName","name"]
	    }
	});  
}else{
	$("#name").attr('readonly',true);
	$("#name").css('background','#eee')
}

//提交表单
$('#mainform').form({    
    onSubmit: function(){    
    	var isValid = $(this).form('validate');
		return isValid;	// 返回false终止表单提交
    },    
    success:function(data){ 
    	var data = eval('(' + data + ')');
    	successTip(data,dg,d);
    }    
});    
</script>
