<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form id="mainform" action="${ctx}/user/updatePwd" method="post">
	<table>
		<tr>
			<td>原密码：</td>
			<td>
			<input id="oldPassword" name="oldPassword" type="password" class="easyui-validatebox" data-options="required:true"/>
			</td>
		</tr>
		<tr>
			<td>密码：</td>
			<td><input id="password" name="password" type="password" class="easyui-validatebox" data-options="required:true,validType:'length[6,20]'"/></td>
		</tr>
		<tr>
			<td>确认密码：</td>
			<td><input id="confirmPassword" name="confirmPassword" type="password" class="easyui-validatebox" validType="equals['password','id']"  data-options="required:true"/></td>
		</tr>
	</table>
</form>
<script>
$(function(){
	$("#oldPassword").focus();
	$('#mainform').form({    
	    onSubmit: function(){    
	    	var isValid = $(this).form('validate');
			return isValid;	// 返回false终止表单提交
	    },    
	    success:function(data){ 
	    	var data = eval('(' + data + ')');
	    	successTip(data,null,d);
	    }    
	});
});
</script>
</body>
</html>