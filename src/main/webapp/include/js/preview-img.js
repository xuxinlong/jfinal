	

    //检查图片的格式是否正确,同时实现预览
    function setImagePreview(obj, localImagId, imgObjPreview,width,height) {
    	localImagId = document.getElementById(localImagId);
    	imgObjPreview = document.getElementById(imgObjPreview);
        var array = new Array('gif', 'jpeg', 'png', 'jpg', 'bmp'); //可以上传的文件类型
        var tmp = obj.value;
        if (tmp == '') {
            $.messager.alert("请选择要上传的图片!");
            return false;
        }
        else {
    		//产品图片
			if (!/\.(gif|jpg|jpeg|png|bmp|GIF|JPG|PNG|BMP)$/.test(tmp)) {
				//alert("图片类型必须是 GIF,JPEG,JPG,PNG,BMP 中的一种");
				$.messager.alert("图片类型必须是 GIF,JPEG,JPG,PNG,BMP 中的一种");
				$(obj).val("");
				return false;
			}
            //图片格式正确之后，根据浏览器的不同设置图片的大小
            if (obj.files && obj.files[0]) {
                //火狐下，直接设img属性 
                imgObjPreview.style.display = 'block';
                imgObjPreview.style.width = width+'px';
                imgObjPreview.style.height = height+'px';
                //火狐7以上版本不能用上面的getAsDataURL()方式获取，需要一下方式 
                imgObjPreview.src = window.URL.createObjectURL(obj.files[0]);
            }
            else {
                //IE下，使用滤镜 
                obj.select();
                var imgSrc = document.selection.createRange().text;
                //必须设置初始大小 
                localImagId.style.width = width+"px";
                localImagId.style.height = height+"px";
                //图片异常的捕捉，防止用户修改后缀来伪造图片 
                try {
                    localImagId.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
                    localImagId.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = imgSrc;
                }
                catch (e) {
                    $.messager.alert("您上传的图片格式不正确，请重新选择!");
                    return false;
                }
                imgObjPreview.style.display = 'none';
                document.selection.empty();
            }
            return true;
        }
    }
			
    
        //显示图片  
        function over(imgid, obj, imgbig) {
    		imgid = document.getElementById(imgid);
    		obj = document.getElementById(obj);
    		imgbig = document.getElementById(imgbig);
    		
    		var img = imgid;
    		
            //大图显示的最大尺寸  4比3的大小  400 300  
            maxwidth = 400;
            maxheight = 300;

            //显示  
            obj.style.display = "";
            imgbig.src = imgid.src;

            //1、宽和高都超过了，看谁超过的多，谁超的多就将谁设置为最大值，其余策略按照2、3  
            //2、如果宽超过了并且高没有超，设置宽为最大值  
            //3、如果宽没超过并且高超过了，设置高为最大值  

            if (img.width > maxwidth && img.height > maxheight) {
                pare = (img.width - maxwidth) - (img.height - maxheight);
                if (pare >= 0)
                    img.width = maxwidth;
                else
                    img.height = maxheight;
            }
            else if (img.width > maxwidth && img.height <= maxheight) {
                img.width = maxwidth;
            }
            else if (img.width <= maxwidth && img.height > maxheight) {
                img.height = maxheight;
            }
        }