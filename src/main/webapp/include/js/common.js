/**
 * ajax返回提示
 * @param data	返回的数据
 * @param dg datagrid
 * @param d	弹窗
 * @returns {Boolean} ajax是否成功
 */
function successTip(data,dg,d){
	
	if(data=='success'||data.errorCode=='y'){
		if(dg!=null)
			dg.datagrid('reload');
		if(d!=null)
			d.panel('close');
		parent.$.messager.show({ title : "提示",msg: "操作成功！", position: "bottomRight" });
		return true;
	}else{
		
		if(data.errorText!=null)
			parent.$.messager.alert(data.errorText);
		else
			parent.$.messager.alert(data);
		return false;
	}  
}
/**
 * 是否选择行数据
 * @param row
 * @returns {Boolean}
 */
function rowIsNull(row){
	if(row){
		return false;
	}else{
		parent.$.messager.show({ title : "提示",msg: "请选择行数据！", position: "bottomRight" });
		return true;
	}
}
/**
 * 时间戳格式化
 * @param now
 * @returns {String}
 */
function formatDate(now){
    var date = new Date(now*1000);
    Y = date.getFullYear() + '-';
    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    D = date.getDate() + ' ';
    h = date.getHours() + ':';
    m = date.getMinutes() + ':';
    s = date.getSeconds(); 
    return Y+M+D+h+m+s;
}