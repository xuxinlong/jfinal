/*
Navicat MySQL Data Transfer

Source Server         : 192.168.2.150
Source Server Version : 50626
Source Host           : 192.168.2.150:3306
Source Database       : xxl

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2016-08-15 08:47:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `x_menu`
-- ----------------------------
DROP TABLE IF EXISTS `x_menu`;
CREATE TABLE `x_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单Id',
  `parentId` int(11) DEFAULT '0' COMMENT '父级id',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `url` varchar(255) DEFAULT NULL COMMENT '菜单地址',
  `menuCode` varchar(50) DEFAULT NULL COMMENT '菜单编码',
  `status` tinyint(2) DEFAULT '1' COMMENT '0.逻辑删除1.可用',
  `description` varchar(50) DEFAULT NULL COMMENT '菜单描述',
  `sortNo` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- ----------------------------
-- Records of x_menu
-- ----------------------------
INSERT INTO `x_menu` VALUES ('1', '0', '系统管理', 'icon-standard-cog', null, null, '1', '系统管理', '1');
INSERT INTO `x_menu` VALUES ('2', '1', '用户管理', 'icon-hamburg-hire-me', 'user', null, '1', '用户管理', '1');
INSERT INTO `x_menu` VALUES ('3', '1', '角色管理', 'icon-hamburg-my-account', 'role', null, '1', '角色管理', '2');
INSERT INTO `x_menu` VALUES ('4', '1', '菜单管理', 'icon-hamburg-old-versions', 'menu', null, '1', '菜单管理', '3');

-- ----------------------------
-- Table structure for `x_role`
-- ----------------------------
DROP TABLE IF EXISTS `x_role`;
CREATE TABLE `x_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `roleCode` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `description` varchar(500) DEFAULT NULL COMMENT '角色描述',
  `status` tinyint(2) DEFAULT '1' COMMENT '状态:0.逻辑删除1.正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Records of x_role
-- ----------------------------
INSERT INTO `x_role` VALUES ('1', '超级管理员', 'root', '超级管理员', '1');
INSERT INTO `x_role` VALUES ('2', '部门经理', 'jingli', '部门经理', '1');

-- ----------------------------
-- Table structure for `x_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `x_role_menu`;
CREATE TABLE `x_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色菜单id',
  `roleId` int(11) DEFAULT NULL COMMENT '角色id',
  `menuId` int(11) DEFAULT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='角色菜单表';

-- ----------------------------
-- Records of x_role_menu
-- ----------------------------
INSERT INTO `x_role_menu` VALUES ('1', '1', '1');
INSERT INTO `x_role_menu` VALUES ('4', '1', '4');
INSERT INTO `x_role_menu` VALUES ('5', '1', '2');
INSERT INTO `x_role_menu` VALUES ('6', '1', '3');
INSERT INTO `x_role_menu` VALUES ('7', '2', '2');

-- ----------------------------
-- Table structure for `x_user`
-- ----------------------------
DROP TABLE IF EXISTS `x_user`;
CREATE TABLE `x_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '用户名',
  `nickName` varchar(100) DEFAULT NULL COMMENT '昵称',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `createTime` int(11) DEFAULT NULL COMMENT '创建时间',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号',
  `lastTime` int(11) DEFAULT NULL COMMENT '上次登录时间',
  `lastIp` varchar(50) DEFAULT NULL COMMENT '上次登录ip地址',
  `info` varchar(500) DEFAULT NULL COMMENT '信息描述',
  `status` tinyint(2) DEFAULT '1' COMMENT '用户状态:0.禁用（逻辑删除）1.正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
-- Records of x_user
-- ----------------------------
INSERT INTO `x_user` VALUES ('1', 'admin', 'admin', '21218cca77804d2ba1922c33e0151105', null, '18305113589', '1471221279', '127.0.0.1', '超级管理员', '1');
INSERT INTO `x_user` VALUES ('2', '12345', '测试', '81DC9BDB52D04DC20036DBD8313ED055', null, '12345678910', null, null, '123666', '1');

-- ----------------------------
-- Table structure for `x_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `x_user_role`;
CREATE TABLE `x_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户角色id',
  `roleId` int(11) DEFAULT NULL COMMENT '角色id',
  `userId` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户角色表';

-- ----------------------------
-- Records of x_user_role
-- ----------------------------
INSERT INTO `x_user_role` VALUES ('1', '2', '1');
INSERT INTO `x_user_role` VALUES ('2', '1', '1');
INSERT INTO `x_user_role` VALUES ('3', '2', '2');
