package com.jfinal.util;

import javax.servlet.http.HttpServletRequest;

/** 
 *	获取ip地址
 * @author 徐新龙
 * @version 2016年8月12日 下午1:21:49 
 * 
 */
public class IpUtil {
		
	public static String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		while (true) {
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("Proxy-Client-IP");
			} else {
				break;
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("WL-Proxy-Client-IP");
			} else {
				break;
			}
			if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip))) {
				ip = request.getHeader("X-Real-IP");
			} else {
				break;
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			} else {
				break;
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("http_client_ip");
			} else {
				break;
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("HTTP_X_FORWARDED_FOR");
			} else {
				break;
			}
			break;
		}

		if (ip != null && ip.indexOf(",") != -1) {
			ip = ip.substring(ip.lastIndexOf(",") + 1, ip.length()).trim();
		}
		if ("0:0:0:0:0:0:0:1".equals(ip)) {
			ip = "127.0.0.1";
		}
		return ip;
	}
}
