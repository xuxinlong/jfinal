package com.jfinal.model;

import com.jfinal.model.base.BaseUser;
import com.jfinal.plugin.activerecord.Page;

/** 
 *	用户
 * @author 徐新龙
 * @version 2016年8月8日 上午10:21:46 
 * 
 */
@SuppressWarnings("serial")
public class User extends BaseUser<User>{
	
	public static final User dao=new User();
	
	/**
	 * 检测登录
	 * @param name
	 * @param password
	 * @return
	 */
	public User checkLogin(String name,String password){
		User user=User.dao.findFirst("select * from x_user where name=? and password=?", name,password);
		return user;
	}
	
	public Page<User> getList(Integer pageNumber,Integer pageSize,String nickName){
		Page<User> userList=User.dao.paginate(pageNumber, pageSize, "select *", "from x_user where status=1");
		System.out.println(userList.getList().get(0));
		return userList;
	}

	public Boolean checkLoginName(String name) {
		User user=dao.findFirst("select * from x_user where name=?", name);
		if(user!=null){
			return false;
		}else{
			return true;
		}
	}
}
