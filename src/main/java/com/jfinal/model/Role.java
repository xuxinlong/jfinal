package com.jfinal.model;

import com.jfinal.model.base.BaseRole;
import com.jfinal.plugin.activerecord.Page;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Role extends BaseRole<Role> {
	public static final Role dao = new Role();

	public Page<Role> getList(Integer pageNumber, Integer pageSize, String name) {
		return paginate(pageNumber, pageSize, "select *", "from x_role where status=1 order by id desc");
	}
	
}
