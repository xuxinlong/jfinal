package com.jfinal.controller.system;

import java.util.HashMap;
import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.model.Menu;

/** 
 *
 * @author 徐新龙
 * @version 2016年8月12日 下午3:34:55 
 * 
 */
public class MenuController extends Controller{
	public void index(){
		render("menuList.jsp");
	}
	/**
	 * list数据
	 */
	public void list(){
		List<Menu> menuList=Menu.dao.getMenuList();
		renderJson(menuList);
	}
	/**
	 * 编辑页面
	 */
	public void edit(){
		Menu menu=Menu.dao.findById(getParaToInt(0));
		setAttr("menu", menu);
		render("menuForm.jsp");
	}
	/**
	 * 保存
	 */
	public void save(){
		HashMap<String,Object> retMap=new HashMap<String, Object>();
		Menu menu=getModel(Menu.class,"");
		try {
			if(menu.getId()==null){
				menu.save();
			}else{
				menu.update();
			}
			retMap.put("errorCode", "y");
			retMap.put("errorText", "保存成功");
		} catch (Exception e) {
			retMap.put("errorCode", "n");
			retMap.put("errorText", "保存失败");
		}
		renderJson(retMap);
	}
	/**
	 * 删除
	 */
	public void delete(){
		HashMap<String,Object> retMap=new HashMap<String, Object>();
		try {
			Menu menu=new Menu();
			menu.setId(getParaToInt(0));
			menu.setStatus(0);
			menu.update();
			retMap.put("errorCode", "y");
			retMap.put("errorText", "删除成功");
		} catch (Exception e) {
			retMap.put("errorCode", "n");
			retMap.put("errorText", "删除失败");
		}
		renderJson(retMap);
	}
}
