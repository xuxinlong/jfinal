package com.jfinal.controller.system;

import java.util.HashMap;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.jfinal.core.Controller;
import com.jfinal.model.Role;
import com.jfinal.model.RoleMenu;
import com.jfinal.plugin.activerecord.Page;

/** 
 *
 * @author 徐新龙
 * @version 2016年8月12日 下午3:34:38 
 * 
 */
public class RoleController extends Controller{
	
	public void index(){
		render("roleList.jsp");
	}
	/**
	 * list数据
	 */
	public void list(){
		Page<Role> roleList=Role.dao.getList(getParaToInt("pageNumber"),getParaToInt("pageSize"),getPara("name"));
		renderJson(roleList);
	}
	/**
	 * 编辑页面
	 */
	public void edit(){
		Role role=Role.dao.findById(getParaToInt(0));
		setAttr("role", role);
		render("roleForm.jsp");
	}
	/**
	 * 保存
	 */
	public void save(){
		HashMap<String,Object> retMap=new HashMap<String, Object>();
		Role role=getModel(Role.class,"");
		try {
			if(role.getId()==null){
				role.save();
			}else{
				role.update();
			}
			retMap.put("errorCode", "y");
			retMap.put("errorText", "保存成功");
		} catch (Exception e) {
			retMap.put("errorCode", "n");
			retMap.put("errorText", "保存失败");
		}
		renderJson(retMap);
	}
	/**
	 * 删除
	 */
	public void delete(){
		HashMap<String,Object> retMap=new HashMap<String, Object>();
		try {
			Role role=new Role();
			role.setId(getParaToInt(0));
			role.setStatus(0);
			role.update();
			retMap.put("errorCode", "y");
			retMap.put("errorText", "删除成功");
		} catch (Exception e) {
			retMap.put("errorCode", "n");
			retMap.put("errorText", "删除失败");
		}
		renderJson(retMap);
	}
	/**
	 * 获取角色下的菜单id集合
	 */
	public void getMenu(){
		List<Integer> menuIds=RoleMenu.dao.getMenuIdByRoleId(getParaToInt(0));
		renderJson(menuIds);
	}
	/**
	 * 更新角色下的菜单
	 */
	public void updateMenu(){
		HashMap<String,Object> retMap=new HashMap<String, Object>();
		try {
			Integer roleId=getParaToInt(0);
			String menuIds=getPara("menuIds");
 			List<Integer> newList=JSON.parseArray(menuIds, int.class);
			List<Integer> oldList=RoleMenu.dao.getMenuIdByRoleId(roleId);
			RoleMenu.dao.updateMenu(roleId, oldList, newList);
			retMap.put("errorCode", "y");
			retMap.put("errorText", "保存成功");
		} catch (Exception e) {
			retMap.put("errorCode", "n");
			retMap.put("errorText", "保存失败");
		}
		renderJson(retMap);
	}
}
