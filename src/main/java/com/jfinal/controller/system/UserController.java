package com.jfinal.controller.system;

import java.util.HashMap;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.jfinal.core.Controller;
import com.jfinal.kit.HashKit;
import com.jfinal.model.User;
import com.jfinal.model.UserRole;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.util.DateFormatUtil;

/** 
 *	系统用户管理
 * @author 徐新龙
 * @version 2016年8月8日 下午7:03:27 
 * 
 */
public class UserController extends Controller{

	public void index(){
		render("userList.jsp");
	}
	/**
	 * list数据
	 */
	public void list(){
		Page<User> userList=User.dao.getList(getParaToInt("pageNumber"),getParaToInt("pageSize"),getPara("nickName"));
		renderJson(userList);
	}
	/**
	 * 编辑页面
	 */
	public void edit(){
		User user=User.dao.findById(getParaToInt(0));
		setAttr("user", user);
		render("userForm.jsp");
	}
	/**
	 * 保存
	 */
	public void save(){
		HashMap<String,Object> retMap=new HashMap<String, Object>();
		User user=getModel(User.class,"");
		try {
			if(user.getId()==null){
				user.setCreateTime(DateFormatUtil.currentTimeStamp());
				user.save();
			}else{
				user.update();
			}
			retMap.put("errorCode", "y");
			retMap.put("errorText", "保存成功");
		} catch (Exception e) {
			retMap.put("errorCode", "n");
			retMap.put("errorText", "保存失败");
		}
		renderJson(retMap);
	}
	/**
	 * 删除
	 */
	public void delete(){
		HashMap<String,Object> retMap=new HashMap<String, Object>();
		try {
			User user=new User();
			user.setId(getParaToInt(0));
			user.setStatus(0);
			user.update();
			retMap.put("errorCode", "y");
			retMap.put("errorText", "删除成功");
		} catch (Exception e) {
			retMap.put("errorCode", "n");
			retMap.put("errorText", "删除失败");
		}
		renderJson(retMap);
	}
	/**
	 * 检测登录名是否存在
	 */
	public void checkLoginName(){
		renderJson(User.dao.checkLoginName(getPara("name")));
	}
	/**
	 * 修改密码页面
	 */
	public void updatePwdPage(){
		render("updatePwd.jsp");
	}
	/**
	 * 修改密码
	 */
	public void updatePwd(){
		HashMap<String,Object> retMap=new HashMap<String, Object>();
		try {
			User user=getSessionAttr("User");
			String password = HashKit.md5(getPara("oldPassword"));
			if (user.getPassword().equals(password)){
				user.setPassword(HashKit.md5(getPara("password")));
				user.update();
				retMap.put("errorCode", "y");
				retMap.put("errorText", "修改密码成功");
			}else{
				retMap.put("errorCode", "n");
				retMap.put("errorText", "原密码验证不成功");
			}
		} catch (Exception e) {
			retMap.put("errorCode", "n");
			retMap.put("errorText", "修改密码失败");
		}
		renderJson(retMap);
	}
	/**
	 * 用户角色授权页面
	 */
	public void userRole(){
		setAttr("userId", getParaToInt(0));
		render("userRoleList.jsp");
	}
	/**
	 * 获取用户下角色id集合
	 */
	public void getRoleIds(){
		List<Integer> roleIds=UserRole.dao.getRoleIdByUserId(getParaToInt(0));
		renderJson(roleIds);
	}
	/**
	 * 更新用户下角色
	 */
	public void updateRole(){
		HashMap<String,Object> retMap=new HashMap<String, Object>();
		try {
			Integer userId=getParaToInt(0);
			String roleIds=getPara("roleIds");
 			List<Integer> newList=JSON.parseArray(roleIds, int.class);
			List<Integer> oldList=UserRole.dao.getRoleIdByUserId(userId);
			UserRole.dao.updateRole(userId, oldList, newList);
			retMap.put("errorCode", "y");
			retMap.put("errorText", "保存成功");
		} catch (Exception e) {
			retMap.put("errorCode", "n");
			retMap.put("errorText", "保存失败");
		}
		renderJson(retMap);
	}
}
