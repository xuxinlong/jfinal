package com.jfinal.controller.login;

import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.model.Menu;
import com.jfinal.model.User;

/**
 * 首页控制类
 * @author 徐新龙
 * @data 2016年8月7日
 */
public class IndexController extends Controller{
	
	public void index(){
		User user=getSessionAttr("User");
		setAttr("user", user);
		List<Menu> menuList = Menu.dao.getMenuListByUserId(user.getId()); 
		setAttr("menuList", menuList);
		renderJsp("index.jsp");
	}
}
