package com.jfinal.controller.login;


import java.util.HashMap;

import com.jfinal.aop.Clear;
import com.jfinal.common.LoginInterceptor;
import com.jfinal.core.Controller;
import com.jfinal.kit.HashKit;
import com.jfinal.model.User;
import com.jfinal.util.DateFormatUtil;
import com.jfinal.util.IpUtil;

/**
 * 登录controller
 * @author 徐新龙
 * @data 2016年8月7日
 */
public class LoginController extends Controller{
	
	@Clear(LoginInterceptor.class)
	public void index(){
		if(getSessionAttr("User")!=null){
			redirect("/index");
		}else{
			render("login.jsp");
		}
	}
	
	@Clear(LoginInterceptor.class)
	public void login(){
		HashMap<String,Object> retMap=new HashMap<String, Object>();
		try {
			String username=getPara("username");
			String password=getPara("password");
			User user=User.dao.checkLogin(username,HashKit.md5(password));
			if(user==null){
				retMap.put("errorCode", "n");
				retMap.put("errorText", "用户名或密码不正确，请重新登录");
			}else{
				setSessionAttr("User", user);
				user.setLastIp(IpUtil.getIpAddress(getRequest()));
				user.setLastTime(DateFormatUtil.currentTimeStamp());
				user.update();
				retMap.put("errorCode", "y");
				retMap.put("errorText", "登录成功");
			}
		} catch (Exception e) {
			retMap.put("errorCode", "n");
			retMap.put("errorText", "登录失败");
		}
		renderJson(retMap);
	}
	
	public void logout(){
		removeSessionAttr("User");
		redirect("/login");
	}
}
