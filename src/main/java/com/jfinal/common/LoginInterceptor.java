package com.jfinal.common;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/** 
 *
 * @author 徐新龙
 * @version 2016年8月8日 上午9:18:00 
 * 
 */
public class LoginInterceptor implements Interceptor{

	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
        //判断登录条件是否成立(除了登录功能不拦截之外，其他都拦截)
        if(controller.getSessionAttr("User") == null){
            controller.redirect("/login");
        }else{
            inv.invoke();
        }
	}

}
