package com.jfinal.common;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

/** 
 *	重复提交拦截器
 * @author 徐新龙
 * @version 2016年8月8日 下午7:08:07 
 * 
 */
public class CheckTokenInterceptor implements Interceptor{
	//配合页面放入表单中${token}
	//控制层验证 boolean flag = this.validateToken("token");//如果验证是true，则表示第一次登录，false表示重复登陆
	public void intercept(Invocation inv) {
		//创建token,这里是一进页面会获得token的一个值，刷新页面值会变化
		inv.getController().createToken("token");
		inv.invoke();
		
	}

}
